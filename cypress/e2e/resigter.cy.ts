describe('Register Flow', () => {
  beforeEach(() => {
    cy.intercept('POST', '**/auth/register', {
      statusCode: 200,
      body: { message: 'Registration successful! Please login.' }
    }).as('registerRequest');
  });

  it('goes to the register page', () => {
    cy.visit('/register');
    cy.url().should('include', '/register');
  });
});
