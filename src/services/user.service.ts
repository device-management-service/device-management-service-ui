import instance from './api';
import { HttpResponse } from '../util/http-response';
import { AuthInfo } from '../interfaces/auth.interface';

export const register = async (username: string, email: string, password: string): Promise<HttpResponse<void>> => {
  try{
    const response = await instance.post<HttpResponse<void>>('auth/register', { username, email, password });
    return response.data;
  }
  catch(err) {
    console.error(err)
    return {
      statusCode: 999,
      message: 'Registration failed!'
    }
  }
};

export const login = async (email: string, password: string): Promise<HttpResponse<AuthInfo | null>> => {
  try{
    const response = await instance.post<HttpResponse<AuthInfo>>('auth/login', { email, password });
    return response.data;
  }
  catch(err){
    console.error(err)
    return {
      statusCode: 999,
      message: 'Login fail!'
    }
  }
};