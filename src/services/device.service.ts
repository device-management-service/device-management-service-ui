import { Device, CreateDevice } from '../interfaces/device.interface';
import { HttpResponse } from '../util/http-response';
import instance from './api';

export const createDevice = async (deviceData: CreateDevice, token: string): Promise<HttpResponse<void>> => {
  try{
    const response = await instance.post<HttpResponse<void>>('/device', deviceData, {
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }
  catch(err: any) {
    return {
      statusCode: 999,
      message: 'Create Device Fail'
    }
  }
}

export const fetchDevices = async (token: string): Promise<HttpResponse<Device[]>> => {
  try{
    const response = await instance.get<HttpResponse<Device[]>>('/device', {
      headers: { Authorization: `Bearer ${token}` }
    });
    return response.data;
  }
  catch(err) {
    console.error(err)
    return {
      statusCode: 999,
      message: 'Fetch Devices Fail',
      data: []
    }
  }
};

export const fetchDeviceOne = async (id: string, token: string): Promise<HttpResponse<Device | null>> => {
  try{
    const response = await instance.get<HttpResponse<Device>>(`/device/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    });

    return response.data;
  }
  catch(err) {
    console.error(err);
    return {
      statusCode: 999,
      message: 'Fetch Device Fail'
    }
  }
}

export const updateDevice = async (id: string, updateData: Partial<Device>, token: string): Promise<HttpResponse<void>> => {
  try{
    const response = await instance.put<HttpResponse<void>>(`/device/${id}`, updateData, {
      headers: { Authorization: `Bearer ${token}` }
    });

    return response.data;
  }
  catch(err) {
    console.error(err);
    return {
      statusCode: 999,
      message: 'Update Device Fail'
    }
  }
}

export async function deleteDevice(id: string, token: string): Promise<HttpResponse<boolean>> {
  try {
    const response = await instance.delete<HttpResponse<boolean>>(`/device/${id}`, {
      headers: { Authorization: `Bearer ${token}` }
    });

    return response.data;
  }
  catch(err) {
    console.error(err);
    return {
      statusCode: 999,
      message: 'Delete Device Fail'
    }
  }
}