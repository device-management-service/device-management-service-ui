export const httpResponseSuccess = 1
export const httpResponseUnauthorized = 401
export const unauthorizedMsg = "Unauthorized"
export const cookieExpires = 1 / 24 // The token expires after 1 hour