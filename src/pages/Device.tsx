import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { deleteDevice, fetchDeviceOne } from '../services/device.service';
import Cookies from 'js-cookie';
import { Device as DeviceInfo} from '../interfaces/device.interface';
import '../styles/Device.css';
import Sidebar from '../components/Sidebar/Sidebar';
import Header from '../components/Header/Header';
import Button from '../components/Button/Button';
import Dropdown from '../components/Dropdown/Dropdown';
import ConfirmDialog from '../components/ConfirmDialog/ConfirmDialog';
import { httpResponseSuccess, httpResponseUnauthorized, unauthorizedMsg } from '../config/constant';

function Device() {
  const { id } = useParams<{ id: string }>();
  const [device, setDevice] = useState<DeviceInfo | null>(null);
  const [showNotification, setShowNotification] = useState(false);
  const [notificationMessage, setNotificationMessage] = useState('');
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);  
  const navigate = useNavigate()

  const username = 'User';

  useEffect(() => {
    const fetchData = async (id: string) => {
        const token = Cookies.get('token') ?? '';
        const response = await fetchDeviceOne(id, token);
        if(response.statusCode === httpResponseSuccess){
          const devices = response.data as DeviceInfo;
          return devices;
        }
        else if(response.statusCode === httpResponseUnauthorized){
          throw new Error(unauthorizedMsg);
        }
        else {
          throw new Error(response.message);
        }
      };

     if (!id) {
      navigate('/device');
    } else {

      fetchData(id)
        .then((device: DeviceInfo) => setDevice(device))
        .catch((err) => {
          if(err.message === unauthorizedMsg){
            navigate('/login');
          }
          else{
            setNotificationMessage(err.message);
            setShowNotification(true);
            setTimeout(() => {
                setShowNotification(false);
            }, 3000);
          }
        })

    }
  }, [id, navigate]);

  const handleEdit = () => {
    navigate(`/device/${id}/edit`, { state: { device } })
  };
  
  const handleDelete = async () => {
    setShowConfirmDialog(true);
  };

  const handleConfirmDelete = async () => {
    if (id) {
      const token = Cookies.get('token') ?? '';
      const response = await deleteDevice(id, token);
        if (response.statusCode === httpResponseSuccess) {
            setNotificationMessage('Device deleted successfully');
            setShowNotification(true);
            setTimeout(() => {
                setShowNotification(false);
                navigate('/device');
            }, 1000);
        }
        else if(response.statusCode === httpResponseUnauthorized){
          navigate('/login');
        }
        else {
            setNotificationMessage(response.message);
            setShowNotification(true);
            setTimeout(() => {
                setShowNotification(false);
            }, 3000);
        }
    }
};

  if(!device) {
    return <div>Loading...</div>
  }

  return (
    <div>
        <Header topic={device.name} username={username} />
        <Sidebar />
        <Dropdown show={showNotification} message={notificationMessage} type='success' />
        <ConfirmDialog
          title="Delete Device"
          message="Are you sure you want to delete this device?"
          open={showConfirmDialog}
          setOpen={setShowConfirmDialog}
          onConfirm={handleConfirmDelete}
        />
        <div className="device-container">
          <div className="device-content">
            {device ? (
              <div>
                <p>ID: {device.id}</p>
                <p>Name: {device.name}</p>
                <p>Type: {device.type}</p>
                <p>Description: {device.description || 'none'}</p>
                <p>Created at: {new Date(device.createdAt).toDateString()}</p>
                <p>Updated at: {new Date(device.updatedAt).toDateString()}</p>
                <div className='button-group'>
                  <Button onClick={handleEdit} label='Edit' type='primary'/>
                  <Button onClick={handleDelete} label='Delete' type='danger'/>
                </div>
              </div>
            ) : (
              <p>Loading...</p>
            )}
          </div>
        </div>
    </div>
  );
}

export default Device;
