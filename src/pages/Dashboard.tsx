import React from 'react';
import '../styles/Dashboard.css'
import { fetchDevices } from '../services/device.service';
import Cookies from 'js-cookie';
import { Device } from '../interfaces/device.interface';
import DeviceCard from '../components/DeviceCard/DeviceCard';
import Sidebar from '../components/Sidebar/Sidebar';
import Dropdown from '../components/Dropdown/Dropdown';
import Header from '../components/Header/Header';
import { useNavigate } from 'react-router-dom';
import { AiOutlinePlus } from 'react-icons/ai';
import { httpResponseSuccess, httpResponseUnauthorized, unauthorizedMsg } from '../config/constant';

function Dashboard() {
    const [devices, setDevices] = React.useState<Device[]>([]);
    const [dropdown, setDropdown] = React.useState({ show: false, message: '', type: '' });

    const navigate = useNavigate();

    const handleAddDevice = () => {
      navigate('/device/new');
    }


    const username = 'User';
    React.useEffect(() => {
      const fetchData = async () => {
        const token = Cookies.get('token') ?? '';
        const response = await fetchDevices(token);
        console.log(response.statusCode)
        if(response.statusCode === httpResponseSuccess){
          const devices = response.data as Device[];
          return devices;
        }
        else if(response.statusCode === httpResponseUnauthorized){
          throw new Error(unauthorizedMsg);
        }
        else {
          throw new Error(response.message);
        }
      };

      fetchData()
        .then((devices: Device[]) => setDevices(devices))
        .catch((err) => {
          if(err.message === unauthorizedMsg){
            navigate('/login')
          }
          else{
            setDropdown({ show: true, message: err.message, type: 'error' });
            setTimeout(() => setDropdown({ show: false, message: '', type: '' }), 3000);
          }
        })
    }, [navigate]);

    return (
    <div>
      <Dropdown show={dropdown.show} message={dropdown.message} type={dropdown.type as 'success' | 'error'} />
      <Header username={username} topic="Device" />
      <Sidebar /> 
      <div className='dashboard-container'>
        {devices.length > 0 ? (
          devices.map(device => (
            <DeviceCard key={device.id} id={device.id} name={device.name} type={device.type} description={device.description || 'None'} />
          ))
        ) : (
          <div className='no-device-message'>
            <p> No devices found. Please add a device.</p>
            <p> At "Plus" icon on right below.</p>
          </div>
        )}
      </div>
      <div className='add-device-button'>
        <AiOutlinePlus onClick={handleAddDevice} className="add-device-icon" /> 
      </div>
    </div>
    );
}

export default Dashboard;