const Error = () => {
  return (
    <div>
      <h1>Error</h1>
      <p>An error has occurred. Please try again later.</p>
    </div>
  );
};

export default Error;