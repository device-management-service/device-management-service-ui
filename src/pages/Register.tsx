import React, { useState } from 'react';
import '../styles/Register.css'
import { register } from '../services/user.service';
import { useNavigate } from 'react-router-dom';
import Dropdown from '../components/Dropdown/Dropdown';
import { httpResponseSuccess } from '../config/constant';

const Register = () => {
const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [dropdown, setDropdown] = useState({ show: false, message: '', type: '' });

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    const response = await register(username, email, password);
    if(response.statusCode === httpResponseSuccess) {
      setDropdown({ show: true, message: 'Registration successful! Please login.', type: 'success' });
      setTimeout(() => { 
        setDropdown({ show: false, message: '', type: '' });
        navigate('/login');
      }, 3000);
    }
    else {
      setDropdown({ show: true, message: response.message, type: 'error' });
      setTimeout(() => setDropdown({ show: false, message: '', type: '' }), 3000);
    }
  };

  return (
    <div className="register-page">
      <Dropdown show={dropdown.show} message={dropdown.message} type={dropdown.type as 'success' | 'error'} />
      <div className="register-card">
        <h2>Register</h2>
        <form onSubmit={handleSubmit}>
          <input 
            type="text"
            id="username_id"
            placeholder="Username"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
          <input 
            type="email"
            id="email_id"
            placeholder="Email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
          <input 
            type="password"
            id="password_id"
            placeholder="Password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <button type="submit" data-cy="submit">Register</button>
        </form>
        <div className="login-option">
          <span>Already have an account? </span>
          <button className="login-button" onClick={() => navigate('/login')}>Login</button>
        </div>
      </div>
    </div>
  );
};

export default Register;
