import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';

function Landing() {
  const navigate = useNavigate();

  useEffect(() => {
    const token = Cookies.get('token');
    if (token) {
      navigate('/device');
    } else {
      navigate('/login');
    }
  }, [navigate]);

  return null;
}

export default Landing;
