import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';
import '../styles/CreateDevice.css';
import Sidebar from '../components/Sidebar/Sidebar';
import Header from '../components/Header/Header';
import Button from '../components/Button/Button';
import Dropdown from '../components/Dropdown/Dropdown';
import { createDevice } from '../services/device.service';
import { httpResponseSuccess, httpResponseUnauthorized } from '../config/constant';

function CreateDevice() {
  const [deviceName, setDeviceName] = useState('');
  const [deviceType, setDeviceType] = useState('');
  const [deviceDescription, setDeviceDescription] = useState('');
  const [dropdown, setDropdown] = useState({ show: false, message: '', type: '' });
  const navigate = useNavigate()

  const username = 'User';

  const handleSave = async (event: React.FormEvent) => {
    event.preventDefault();
    const token = Cookies.get('token') ?? '';
    const deviceData = {
      name: deviceName,
      type: deviceType,
      description: deviceDescription,
    }
  
    const response = await createDevice(deviceData, token);
    if(response.statusCode === httpResponseSuccess){
      setDropdown({ show: true, message: 'Create Device successful!.', type: 'success' });
      setTimeout(() => {
        setDropdown({ show: false, message: '', type: '' });
        navigate('/device');
      }, 1000)
    }
    else if(response.statusCode === httpResponseUnauthorized){
      navigate('/login');
    }
    else {
      setDropdown({ show: true, message: response.message, type: 'error' })
      setTimeout(() => setDropdown({ show: false, message: '', type: '' }), 3000);
    }
  };

  const handleCancel = () => {
    navigate('/device');
  };

  return (
    <div>
      <Dropdown show={dropdown.show} message={dropdown.message} type={dropdown.type as 'success' | 'error'} />
      <Header topic={`Create new device`} username={username} />
      <Sidebar />
      <div className="create-device-container">
        <div className="create-device-content">
          <form>
            <div className="form-group">
              <label className="form-label" htmlFor="deviceName">Device Name</label>
              <input 
                className="form-input" 
                id="deviceName" 
                type="text" 
                value={deviceName} 
                onChange={e => setDeviceName(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="deviceType">Device Type</label>
              <input 
                className="form-input" 
                id="deviceType" 
                type="text" 
                value={deviceType} 
                onChange={e => setDeviceType(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="deviceDescription">Device Description</label>
              <textarea 
                className="form-input" 
                id="deviceDescription" 
                value={deviceDescription} 
                onChange={e => setDeviceDescription(e.target.value)}
              />
            </div>
            <div className="button-group">
              <Button onClick={handleCancel} label='Cancel' type='secondary'/>
              <Button onClick={handleSave} label='Save' type='primary'/>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateDevice;
