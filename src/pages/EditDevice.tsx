import React, { useEffect, useState } from 'react';
import { useParams, useNavigate, useLocation } from 'react-router-dom';
import '../styles/EditDevice.css';
import Sidebar from '../components/Sidebar/Sidebar';
import Header from '../components/Header/Header';
import Button from '../components/Button/Button';
import Cookies from 'js-cookie';
import { updateDevice } from '../services/device.service';
import { httpResponseSuccess, httpResponseUnauthorized } from '../config/constant';
import Dropdown from '../components/Dropdown/Dropdown';

function EditDevice() {
  const { id } = useParams<{ id: string }>();
  const location = useLocation();
  const [deviceName, setDeviceName] = useState(location.state?.device.name || null);
  const [deviceType, setDeviceType] = useState(location.state?.device.type || null);
  const [deviceDescription, setDeviceDescription] = useState(location.state?.device.description || null);
  const [dropdown, setDropdown] = useState({ show: false, message: '', type: '' });
  const navigate = useNavigate()

  const username = 'User';

  useEffect(() => {
    setDeviceName(deviceName)
    setDeviceType(deviceType)
    setDeviceDescription(deviceDescription)
  }, [deviceName, deviceType, deviceDescription]);

  const handleSave = async (event: React.FormEvent) => {
    event.preventDefault();
    const token = Cookies.get('token') ?? '';
    const deviceData = {
      name: deviceName,
      type: deviceType,
      description: deviceDescription,
    }

    if(!id){
      navigate('/device');
    }
    else{
      const response = await updateDevice(id, deviceData, token);
      if(response.statusCode === httpResponseSuccess){
        setDropdown({ show: true, message: 'Ureate Device successful!.', type: 'success' });
        setTimeout(() => {
          setDropdown({ show: false, message: '', type: '' });
          navigate('/device');
        }, 1000)
      }
      else if(response.statusCode === httpResponseUnauthorized){
        navigate('/login');
      }
      else {
        setDropdown({ show: true, message: response.message, type: 'error' })
        setTimeout(() => setDropdown({ show: false, message: '', type: '' }), 3000);
      }
    }
    
  };

  const handleCancel = () => {
    navigate(`/device/${id}`);
  };

  return (
    <div>
      <Dropdown show={dropdown.show} message={dropdown.message} type={dropdown.type as 'success' | 'error'} />
      <Header topic={`Edit: ${deviceName}`} username={username} />
      <Sidebar />
      <div className="edit-device-container">
        <div className="edit-device-content">
          <form>
            <div className="form-group">
              <label className="form-label" htmlFor="deviceName">Device Name</label>
              <input className="form-input" id="deviceName" type="text" defaultValue={deviceName} onChange={e => setDeviceName(e.target.value)} />
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="deviceType">Device Type</label>
              <input className="form-input" id="deviceType" type="text" defaultValue={deviceType} onChange={e => setDeviceType(e.target.value)} />
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="deviceDescription">Device Description</label>
              <textarea className="form-input" id="deviceDescription" defaultValue={deviceDescription} onChange={e => setDeviceDescription(e.target.value)}></textarea>
            </div>
            <div className="button-group">
              <Button onClick={handleCancel} label='Cancel' type='secondary'/>
              <Button onClick={handleSave} label='Save' type='primary'/>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditDevice;
