import React from 'react';
import '../styles/Login.css';
import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';
import { login } from '../services/user.service';
import Dropdown from '../components/Dropdown/Dropdown';
import { cookieExpires, httpResponseSuccess } from '../config/constant';
import { AuthInfo } from '../interfaces/auth.interface';

function Login() {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [dropdown, setDropdown] = React.useState({ show: false, message: '', type: '' });
    const navigate = useNavigate();

    const handleLogin = async (event: React.FormEvent) => {
        event.preventDefault();
        const response = await login(email, password);
        if(response.statusCode === httpResponseSuccess) {
            const authInfo = response.data as AuthInfo;
            Cookies.set('token', authInfo.accessToken, { expires: cookieExpires });
            setTimeout(() => setDropdown({ show: false, message: '', type: '' }), 3000);
            navigate('/device');
        }
        else {
            setDropdown({ show: true, message: response.message, type: 'error' });
            setTimeout(() => setDropdown({ show: false, message: '', type: '' }), 3000);
        }
    };

    return (
        <div className="login-page">
            <Dropdown show={dropdown.show} message={dropdown.message} type={dropdown.type as 'success' | 'error'} />
            <div className="login-card">
                <h2>Login</h2>
                <form onSubmit={handleLogin}>
                    <input type="text" value={email} onChange={e => setEmail(e.target.value)} placeholder="Email" />
                    <input type="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="Password" />
                    <button type="submit">Login</button>
                </form>
                <div className="register-option">
                    <span>Don't have an account? </span>
                    <button onClick={() => navigate('/register')} className="register-button">Register</button>
                </div>
            </div>
        </div>
    );
}

export default Login;
