import React from "react";
import "./ConfirmDialog.css";

interface ConfirmDialogProps {
  title: string;
  message: string;
  open: boolean;
  setOpen: (open: boolean) => void;
  onConfirm: () => void;
}

const ConfirmDialog: React.FC<ConfirmDialogProps> = ({ title, message, open, setOpen, onConfirm }) => {
  if (!open) return null;

  const handleConfirm = () => {
    onConfirm();
    setOpen(false);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  return (
    <div className="confirm-dialog-container">
      <div className="confirm-dialog-box">
        <h2 className="confirm-dialog-title">{title}</h2>
        <p className="confirm-dialog-message">{message}</p>
        <div className="confirm-dialog-buttons">
          <button onClick={handleCancel} className="confirm-dialog-cancel">Cancel</button>
          <button onClick={handleConfirm} className="confirm-dialog-confirm">Confirm</button>
        </div>
      </div>
    </div>
  );
};

export default ConfirmDialog;
