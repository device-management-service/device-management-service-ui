import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import './Dropdown.css';

interface DropdownProps {
  show: boolean;
  message: string;
  type: 'success' | 'error';
}

const Dropdown: React.FC<DropdownProps> = ({ show, message, type }) => {
  const [active, setActive] = useState(show);

  useEffect(() => {
    setActive(show);
  }, [show]);

  return (
    <CSSTransition
      in={active}
      timeout={300}
      classNames="dropdown"
      unmountOnExit
      onExited={() => setActive(false)}
    >
      <div className={`dropdown ${type}`}>
        {message}
      </div>
    </CSSTransition>
  );
};

export default Dropdown;