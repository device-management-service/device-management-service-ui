import UserCard from "../UserCard/UserCard";
import "./Header.css"

interface HeaderProps {
  topic: string
  username: string;
}

const Header: React.FC<HeaderProps> = ({topic, username}) => {
  return (
    <div className="header-container">
      <h2>{topic}</h2>
      <UserCard username={username} />
    </div>
  )
}

export default Header;