import React from 'react';
import { useNavigate } from 'react-router-dom';
import './DeviceCard.css'; // we'll create this next

interface CardProps {
  id: string;
  name: string;
  type: string;
  description: string;
}

const DeviceCard: React.FC<CardProps> = ({ id, name, type, description }) => {
  let navigate = useNavigate();

  const handleClick = () => {
    navigate(`/device/${id}`);
  };

  return (
    <div className="card" onClick={handleClick}>
      <div className="card-content">
        <h2>{name}</h2>
        <h3>{type}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
};

export default DeviceCard;