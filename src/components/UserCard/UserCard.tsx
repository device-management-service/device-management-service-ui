import React from 'react';
import { FaUserCircle } from 'react-icons/fa';
import './UserCard.css';

interface UserCardProps {
  username: string;
}

const UserCard: React.FC<UserCardProps> = ({ username }) => {
  return (
    <div className="user-card">
      <FaUserCircle size={32} />
      <div className="username">{username}</div>
    </div>
  );
};

export default UserCard;