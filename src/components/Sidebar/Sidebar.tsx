import React from 'react';
import { useNavigate } from 'react-router-dom';
import './Sidebar.css';

const Sidebar: React.FC = () => {
  const navigate = useNavigate();

  const handleLogout = () => {
    // Clear the user's token from cookies
    document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    navigate('/login');
  };

  return (
    <div className="sidebar">
      <div className="menu-item" onClick={() => navigate('/device')}>
        Device
      </div>
      <div className="menu-item" onClick={handleLogout}>
        Logout
      </div>
    </div>
  );
};

export default Sidebar;
