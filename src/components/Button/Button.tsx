import React from 'react';
import './Button.css';

type ButtonProps = {
  onClick: (e?: any) => void,
  label: string,
  type?: 'primary' | 'secondary' | 'danger' | 'success' | 'info' | 'warning'
};

const Button: React.FC<ButtonProps> = ({ onClick, label, type = 'primary' }) => {
  return (
    <button className={`button ${type}`} onClick={onClick}>
      {label}
    </button>
  );
};

export default Button;
