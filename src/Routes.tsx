import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Landing from './pages/Landing';
import Register from './pages/Register';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Device from './pages/Device';
import CreateDevice from './pages/CreateDevice';
import EditDevice from './pages/EditDevice';
import Error from './pages/Error';

const AppRoutes: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />}/>
        <Route path="/device" element={<Dashboard />}/>
        <Route path="/device/new" element={<CreateDevice />}/>
        <Route path="/device/:id" element={<Device />} />
        <Route path="/device/:id/edit" element={<EditDevice />} />
        <Route path="/error" element={<Error />} />
      </Routes>
    </Router>
  );
};

export default AppRoutes;
