export interface AuthInfo {
  accessToken: string;
  username: string;
}