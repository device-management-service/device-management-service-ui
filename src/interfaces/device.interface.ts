export interface Device {
  id: string;
  name: string;
  type: string;
  description: string;
  ownerBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}

export interface CreateDevice {
  name: string;
  type: string;
  description: string;
}